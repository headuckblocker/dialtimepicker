package picker.ugurtekbas.com.Picker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Xfermode;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import picker.ugurtekbas.com.library.R;
/**
 * Created by ugurtekbas on 10.05.2015.
 * Modified by headuck 07.2016
 */
public class Picker extends View {

    private final Paint paint;
    private final RectF rectF;
    private final Xfermode dialXferMode = new PorterDuffXfermode(PorterDuff.Mode.DST_IN);

    private float min;
    private float radius;
    private float dialRadius;
    private float offset;

    private DialInfo[] dials;

    private int textColor = Color.BLACK;
    private int clockColor = Color.parseColor("#0f9280");


    private int canvasColor = Color.TRANSPARENT;
    private int trackSize = -1, dialRadiusDP = -1;

    private boolean disableTouch, dialHourFormat, firstRun = true;
    private boolean displayHourFormat;

    private String[] amPmStrings;
    private Rect boundsTime, boundsAmPm, boundsSpace;
    private int amPmPos;
    private Typeface clockTypeface;

    private class DialInfo {
        float slopX;
        float slopY;
        float dialX;
        float dialY;

        int hour;
        int minutes;
        int previousHour;
        boolean amPm;
        double angle, degrees;

        int dialColor = Color.parseColor("#FF9F5B");
        boolean enabled;
        boolean isMoving;
        boolean isDirty = false;

        public DialInfo() {
            angle = (-Math.PI / 2) + .001;
            amPm = Calendar.getInstance().get(Calendar.AM_PM) == 0;
        }

        void initTime(int hour, int minutes, boolean enabled) {
            this.enabled = enabled;
            if (!enabled) return;

            this.minutes = minutes;

            if (dialHourFormat) {
                degrees = ((hour % 24) * 15) + ((minutes % 60) / 4);
            } else {
                amPm = !(hour >= 12);
                degrees = ((hour % 12) * 30) + ((minutes % 60) / 2);
                hour %= 12;
                if (hour == 0) hour = 12;
            }
            this.hour = hour;
            angle = Math.toRadians(degrees) - (Math.PI / 2);
            previousHour = hour;
        }

        void updateTimeFromAngle() {
            if (!enabled) return;
            if (!isDirty) return;
            // this.angle = angle;
            //Rad to Deg
            degrees = (Math.toDegrees(angle) + 90) % 360;
            degrees = (degrees + 360) % 360;

            //get AM/PM
            if (dialHourFormat) {
                hour = ((int) degrees / 15) % 24;
                minutes = ((int) (degrees * 4)) % 60;

            } else {
                hour = ((int) degrees / 30) % 12;
                if (hour == 0) hour = 12;
                //get Minutes
                minutes = ((int) (degrees * 2)) % 60;

                //AM-PM
                if ((hour == 12 && previousHour == 11) || (hour == 11 && previousHour == 12)) {
                    amPm = !amPm;
                }
            }

            // Align minutes to nearest 5 minutes
            minutes = ((minutes+2)/5 * 5);
            if (minutes >= 60) {
                minutes %= 60;
                hour ++;
                if (dialHourFormat) {
                    hour %= 24;
                } else {
                    if (hour == 12) {
                        amPm = !amPm;
                    } else {
                        if (hour > 12) {
                            hour %= 12;
                        }
                    }
                }
            }

            previousHour = hour;

        }

        private void calculatePointerPosition() {
            if (!enabled) return;
            dialX = (float) (radius * Math.cos(angle));
            dialY = (float) (radius * Math.sin(angle));
        }

        int getCurrentHour() {
            if (!enabled) return -1;
            int currentHour = hour;
            if (dialHourFormat) {
                return currentHour;
            } else {
                if (amPm) {
                    if (currentHour == 12) {
                        currentHour = 0;
                    }
                } else {
                    //PM
                    if (currentHour < 12) {
                        currentHour += 12;
                    }
                }
                return currentHour;
            }
        }

        int getCurrentMin() {
            if (!enabled) return -1;
            return minutes;
        }

        String getTimeText() {

            int retHour = hour;
            if (!enabled) return "";

            if (displayHourFormat) {
                retHour = getCurrentHour();
            } else {
                if (dialHourFormat) {
                    retHour = hour % 12;
                    if (retHour == 0) retHour = 12;
                }
            }

            StringBuilder sb = new StringBuilder();
            if ((displayHourFormat) && (retHour < 10)) sb.append("0");
            sb.append(retHour).append(":");
            if (minutes < 10) sb.append("0");
            sb.append(minutes);

            return sb.toString();
        }

        String getAmPmStr() {
            boolean retAmPm = amPm;

            if (!enabled) return "";
            if (displayHourFormat) {
                return ("");
            } else {
                if (dialHourFormat) {
                    retAmPm = !(hour >= 12);
                }
                return (amPmStrings[retAmPm ? 0 : 1]);
            }
        }
    }

    private TimeChangedListener timeListener;

    public Picker(Context context) {
        this(context, null);
    }

    public Picker(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Picker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setTextAlign(Paint.Align.LEFT);

        rectF = new RectF();

        dialHourFormat = true;
        displayHourFormat = DateFormat.is24HourFormat(getContext());

        amPmStrings = getAmPmStrings(context);
        amPmPos = getAmPmPos(context);

        boundsTime = new Rect();
        boundsAmPm = new Rect();
        boundsSpace = new Rect();

        clockTypeface = Typeface.create("sans-serif-light", Typeface.NORMAL);

        dials = new DialInfo[2];
        dials[0] = new DialInfo();
        dials[1] = new DialInfo();

        /* angle0 = dials[0].angle;
        angle1 = dials[1].angle;
        */

        loadAppThemeDefaults();
        loadAttributes(attrs);

    }

    /**
     * Set default theme attributes for picker
     * Theese will be used if picker's attributes are'nt set
     */
    private void loadAppThemeDefaults() {
        TypedValue typedValue = new TypedValue();

        TypedArray a = getContext().obtainStyledAttributes(typedValue.data, new int[]{
                R.attr.colorAccent,
                R.attr.colorPrimary,
                android.R.attr.textColorPrimary,
                R.attr.colorControlNormal});

        dials[0].dialColor = a.getColor(0, dials[0].dialColor);
        dials[1].dialColor = a.getColor(1, dials[1].dialColor);
        textColor = a.getColor(2, textColor);
        clockColor = a.getColor(3, clockColor);

        a.recycle();
    }

    /**
     * Set picker's attributes from xml file
     * @param attrs
     */
    private void loadAttributes(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Picker);

            if (typedArray != null) {
                textColor = typedArray.getColor(R.styleable.Picker_textColor, textColor);
                dials[0].dialColor = typedArray.getColor(R.styleable.Picker_dialColor, dials[0].dialColor);
                dials[1].dialColor = typedArray.getColor(R.styleable.Picker_altDialColor, dials[1].dialColor);
                clockColor = typedArray.getColor(R.styleable.Picker_clockColor, clockColor);
                canvasColor = typedArray.getColor(R.styleable.Picker_canvasColor, canvasColor);
                dialHourFormat = typedArray.getBoolean(R.styleable.Picker_dialHourFormat, dialHourFormat);
                displayHourFormat = typedArray.getBoolean(R.styleable.Picker_clockHourFormat, displayHourFormat);
                trackSize = typedArray.getDimensionPixelSize(R.styleable.Picker_trackSize, trackSize);
                dialRadiusDP = typedArray.getDimensionPixelSize(R.styleable.Picker_dialRadius, dialRadiusDP);

                typedArray.recycle();
            }
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        float width = MeasureSpec.getSize(widthMeasureSpec);
        float height = MeasureSpec.getSize(heightMeasureSpec);

        min = Math.min(width, height);
        setMeasuredDimension((int) min, (int) min);

        offset = min * 0.5f;
        float padding = min / 20;
        radius = min / 2 - (padding * 2);
        dialRadius = dialRadiusDP != -1 ? dialRadiusDP : radius / 7;
        rectF.set(-radius, -radius, radius, radius);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.translate(offset, offset);
        canvas.drawColor(canvasColor);

        if (firstRun) {
            Calendar cal = Calendar.getInstance();
            int minutes = cal.get(Calendar.MINUTE);
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            initTime(hour, minutes, true, (hour+12)%24, minutes, true);
        } else {
            dials[0].updateTimeFromAngle();
            dials[1].updateTimeFromAngle();
        }

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(textColor);
        paint.setAlpha(isEnabled() ? paint.getAlpha() : 77);

        paint.setTypeface(clockTypeface);
        float textBaseline;
        float leftPos;

        //the text which shows time
        String timeText = dials[0].getTimeText();
        String amPmStr = dials[0].getAmPmStr();

        int timeTextDivider = 7; // 5
        int amPmTextDivider = 14; // 10

        paint.setTextSize(min / timeTextDivider);
        int timeTextLen = timeText.length();
        paint.getTextBounds(timeText, 0, timeTextLen, boundsTime);

        textBaseline = paint.getTextSize() / 4;

        paint.setTextSize(min / amPmTextDivider);
        paint.getTextBounds(amPmStr, 0, amPmStr.length(), boundsAmPm);

        paint.getTextBounds("O", 0, 1, boundsSpace);
        float separation = ((float) boundsSpace.width()) / 2;

        leftPos = - (boundsTime.width() + boundsAmPm.width() + separation) / 2;

        if (amPmPos == 1) {
            paint.setTextSize(min / timeTextDivider);
            canvas.drawText(timeText, leftPos, textBaseline, paint);

            paint.setTextSize(min / amPmTextDivider);
            canvas.drawText(amPmStr, leftPos + boundsTime.width() + separation, textBaseline, paint);

        } else {
            canvas.drawText(amPmStr, leftPos, textBaseline, paint);

            paint.setTextSize(min / timeTextDivider);
            //the text which shows time
            canvas.drawText(timeText, leftPos + boundsAmPm.width()  + separation, textBaseline, paint);
        }

        //clocks dial
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(trackSize != -1 ? trackSize : min / 25);
        paint.setColor(clockColor);
        paint.setAlpha(isEnabled() ? paint.getAlpha() : 77);
        canvas.drawOval(rectF, paint);
        /////////////////////////////

        //small circle t adjust time
        dials[0].calculatePointerPosition();
        dials[1].calculatePointerPosition();

        /*for (int i=1; i>=0; i--) {
            DialInfo d = dials[i];
            if (!d.enabled) continue;
            paint.setStyle(Paint.Style.FILL);
            paint.setAlpha(0);
            paint.setXfermode(dialXferMode);
            canvas.drawCircle(d.dialX+1, d.dialY+1, dialRadius+2, paint);
        }*/
        paint.setShadowLayer(4, 1, 3, Color.DKGRAY);
        paint.setStyle(Paint.Style.FILL);
        for (int i=1; i>=0; i--) {
            DialInfo d = dials[i];
            if (!d.enabled) continue;
            paint.setColor(d.dialColor);
            paint.setAlpha(isEnabled() ? paint.getAlpha() : 77);
            paint.setXfermode(null);
            canvas.drawCircle(d.dialX, d.dialY, dialRadius, paint);
        }
        paint.setShadowLayer(0, 1, 2, Color.DKGRAY);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (disableTouch || !isEnabled()) return false;

        getParent().requestDisallowInterceptTouchEvent(true);

        float posX = event.getX() - offset;
        float posY = event.getY() - offset;
        boolean matched = false;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                for (DialInfo d : dials) {
                    if (!d.enabled) continue;
                    d.calculatePointerPosition();
                    if (posX >= (d.dialX - dialRadius) &&
                            posX <= (d.dialX + dialRadius) &&
                            posY >= (d.dialY - dialRadius) &&
                            posY <= (d.dialY + dialRadius)) {

                        d.slopX = posX - d.dialX;
                        d.slopY = posY - d.dialY;
                        d.isMoving = true;
                        invalidate();
                        matched = true;
                        break;
                    }
                }
                if (!matched) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                    return false;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                for (int i = 0; i < 2; i++) {
                    if (dials[i].isMoving && dials[i].enabled) {
                        dials[i].angle = (float) Math.atan2(posY - dials[i].slopY, posX - dials[i].slopX);
                        dials[i].isDirty = true;
                        if (timeListener != null) {
                            timeListener.timeChanged(getTime(i+1));
                        }
                        invalidate();
                        matched = true;
                        break;
                    }
                }
                if (!matched) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                    return false;
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                for (DialInfo d : dials) {
                    d.isMoving = false;
                }
                invalidate();
                performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                break;
        }

        return true;
    }

    private void ensureIndex (int i) {
        if (i < 1 || i > 2) {
            throw new IndexOutOfBoundsException("Index should be from 1 to 2");
        }
    }

    public int getCurrentHour(int i) {
        ensureIndex(i);
        return dials[i-1].getCurrentHour();
    }

    public int getCurrentMin(int i) {
        ensureIndex(i);
        return dials[i-1].getCurrentMin();
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
        invalidate();
    }

    public void setClockColor(int clockColor) {
        this.clockColor = clockColor;
        invalidate();
    }

    public void setDial1Color(int dialColor) {
        dials[0].dialColor = dialColor;
        invalidate();
    }

    public void setDial2Color(int dialColor) {
        dials[1].dialColor = dialColor;
        invalidate();
    }

    public void setCanvasColor(int canvasColor) {
        this.canvasColor = canvasColor;
        invalidate();
    }

    /**
     * To set dial's size
     * @param trackSize
     */
    public void setTrackSize(int trackSize) {
        this.trackSize = trackSize;
    }

    /**
     * To set adjuster's size
     * @param dialRadiusDP
     */
    public void setDialRadiusDP(int dialRadiusDP) {
        this.dialRadiusDP = dialRadiusDP;
    }

    /**
     * To diasble/enable the picker
     * @param disableTouch
     */
    public void disableTouch(boolean disableTouch) {
        this.disableTouch = disableTouch;
    }

    public void setDialHourFormat(boolean format) {
        this.dialHourFormat = format;
    }

    public void setDisplayHourFormat(boolean format) {
        this.displayHourFormat = format;
    }

    /* public String getAmPM() {
        return this.amPmStr;
    }
    */

    @Nullable public Date getTime(int i) {
        ensureIndex(i);
        if (!dials[i-1].enabled) return null;

        Calendar calendar = Calendar.getInstance();
        int tmp = dials[i-1].hour;

        if (!dials[i-1].amPm) {
            if (tmp < 12) tmp += 12;
        } else {
            if (tmp == 12) tmp = 0;
        }

        calendar.set(Calendar.HOUR_OF_DAY, tmp);
        calendar.set(Calendar.MINUTE,  dials[i-1].minutes);
        return calendar.getTime();
    }

    public void setTimeChangedListener(TimeChangedListener timeChangedListener) {
        this.timeListener = timeChangedListener;
    }

    /***
     * Use this method to set picker's time
     *
     * @param hour
     * @param minutes
     */
    public void setTime(int hour, int minutes) {
        setTime(hour, minutes, -1, -1);
    }

    public void setTime(int hour1, int minutes1, int hour2, int minutes2){
        boolean enabled1 = (hour1 != -1) && (minutes1 != -1);
        boolean enabled2 = (hour2 != -1) && (minutes2 != -1);
        initTime(hour1, minutes1, enabled1, hour2, minutes2, enabled2);
        this.invalidate();
    }

    /***
     * Use this method to initialize picker's time
     */
    private void initTime(int hour1, int minutes1, boolean enabled1,
                         int hour2, int minutes2, boolean enabled2) {
        dials[0].initTime(hour1, minutes1, enabled1);
        dials[1].initTime(hour2, minutes2, enabled2);
        firstRun = false;
    }



    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        return new SavedState(superState, getCurrentHour(1), getCurrentMin(1),
                getCurrentHour(2), getCurrentMin(2));
    }
    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        setTime (ss.getHour1(), ss.getMinute1(), ss.getHour2(), ss.getMinute2());
    }

    /**
     * Used to save / restore state of time picker
     *
     */
    private static class SavedState extends View.BaseSavedState {
        private final int mHour1;
        private final int mMinute1;
        private final int mHour2;
        private final int mMinute2;

        private SavedState(Parcelable superState, int hour1, int minute1, int hour2, int minute2) {
            super(superState);
            mHour1 = hour1;
            mMinute1 = minute1;
            mHour2 = hour2;
            mMinute2 = minute2;
        }
        private SavedState(Parcel in) {
            super(in);
            mHour1 = in.readInt();
            mMinute1 = in.readInt();
            mHour2 = in.readInt();
            mMinute2 = in.readInt();
        }
        public int getHour1() {
            return mHour1;
        }
        public int getMinute1() {
            return mMinute1;
        }
        public int getHour2() {
            return mHour2;
        }
        public int getMinute2() {
            return mMinute2;
        }
        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(mHour1);
            dest.writeInt(mMinute1);
            dest.writeInt(mHour2);
            dest.writeInt(mMinute2);
        }
        @SuppressWarnings({"unused", "hiding"})
        public static final Parcelable.Creator<SavedState> CREATOR = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    /*
     * Utility functions
     */

    /**
     * Return the AM PM String for the current config locale
     * @param context
     * @return am / pm string array
     */
    public static String[] getAmPmStrings(Context context) {
        String[] ampm = new DateFormatSymbols(context.getResources().getConfiguration().locale).getAmPmStrings();
        return ampm;
    }

    /**
     * Return the position of am/pm string for the current config locale in the 12hr format
     * @param context
     * @return 0 for prefix AM/PM, 1 otherwise
     */
    public static int getAmPmPos(Context context) {
        String timePattern = null;
        Locale locale = context.getResources().getConfiguration().locale;
        if (Build.VERSION.SDK_INT >= 18) {
            timePattern = DateFormat.getBestDateTimePattern(locale, "hm");
        } else {
            java.text.DateFormat timeFormat =
                    java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT, locale);
            if (timeFormat instanceof SimpleDateFormat)
                timePattern = ((SimpleDateFormat) timeFormat).toPattern();
        }

        if (timePattern == null || timePattern.startsWith("a"))
            return 0;
        return 1;
    }
}